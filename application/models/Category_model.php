<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

	public function select_all() {		
		$data = $this->db->get("category");
		return $data->result();
	}

	public function select_by_id($id) {
		$this->db->where('category_id',$id);
		$data = $this->db->get("category");		
		return $data->row();
	}

	public function insert($data) {
		$data = array(
			'name' => $data['categoryname'],
			'last_update' => date(DATE_RFC822, time())
		);
		$this->db->insert('category', $data);
		return $this->db->affected_rows();
	}

	public function insert_batch($data) {
		$this->db->insert_batch('category', $data);
		return $this->db->affected_rows();
	}

	public function update($data) {
		$list = array(
			'name' => $data['categoryname'],
			'last_update' => date(DATE_RFC822, time())
		);
		$this->db->where('category_id',$data['category_id']);
		$this->db->update('category', $list);				
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where('category_id', $id);
		$this->db->delete('category');
		return $this->db->affected_rows();
	}

	public function check_nama($nama) {
		$this->db->where('name', $nama);
		$data = $this->db->get('category');
		return $data->num_rows();
	}

	public function total_rows() {
		$data = $this->db->get('category');
		return $data->num_rows();
	}
}

/* End of file M_kota.php */
/* Location: ./application/models/M_kota.php */