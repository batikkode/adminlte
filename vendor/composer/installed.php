<?php return array(
    'root' => array(
        'name' => 'codeigniter/framework',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '2dd9cda760aee0af6b864ab67fc909522f843716',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'codeigniter/framework' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '2dd9cda760aee0af6b864ab67fc909522f843716',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
